#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


void handler(int signum)
{
    int wstatus;
    while (1) {
        pid_t pid = waitpid(-1, &wstatus, WNOHANG | WUNTRACED);
        if (pid < 0) {
            break;
        }
        printf("Child exited %d with status %d\n", pid, WEXITSTATUS(wstatus));
    }
}


int main(int argc, char *argv[])
{
    signal(SIGCHLD, handler);
    while (1) {
        if (fork() == 0) {
            sleep(2);
            exit(0);
        } else {
            sleep(3);
        }
    }
}
