#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

void long_function()
{
    for (int i = 0; i < 100; i++) {
        printf("%d ", i);
        fflush(stdout);
        for (int j = 0; j < 1000; j++) {
            for (int k = 0; k < 10000; k++) {}
        }
    }
}

void none_handler(int signum) {}


void chld_handler(int signum)
{
    int wstatus;
    while (1) {
        pid_t pid = waitpid(-1, &wstatus, WNOHANG | WUNTRACED);
        if (pid < 0) {
            // caught = 1;
            break;
        }
        printf("Child exited %d with status %d\n", pid, wstatus);
    }
}


int main(int argc, char *argv[])
{
    pid_t parent_pid = getpid();
    sigset_t set, oldset;
    memset(&set, 0, sizeof(sigset_t));
    struct sigaction act;
    memset(&act, 0, sizeof(struct sigaction));
    act.sa_flags = SA_SIGINFO;
    sigemptyset(&act.sa_mask);
    int child_pid = fork();
    if (child_pid) {
        act.sa_handler = chld_handler;
        sigaddset(&act.sa_mask, SIGCHLD);
        sigaction(SIGCHLD, &act, NULL);
        printf("parent: begin\n");
        sigfillset(&set);
        sigprocmask(SIG_SETMASK, &set, &oldset);
        long_function();
        sigprocmask(SIG_SETMASK, &oldset, NULL);
        kill(child_pid, SIGUSR1);
        sigfillset(&set);
        sigdelset(&set, SIGCHLD);
        sigsuspend(&set);
    }
    else {
        act.sa_handler = none_handler;
        sigaddset(&act.sa_mask, SIGUSR1);
        sigaction(SIGUSR1, &act, NULL);
        sigfillset(&set);
        sigdelset(&set, SIGUSR1);
        sigsuspend(&set);
        sigprocmask(SIG_SETMASK, &set, &oldset);
        printf("\nchild: begin\n");
        long_function();
        printf("\nchild: end\n");
        sigprocmask(SIG_SETMASK, &oldset, NULL);
        exit(0);
    }
}
