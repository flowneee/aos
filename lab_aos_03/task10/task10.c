#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void sigint_handler(int signum) {
    printf("SIGINT\n");
    for (int i = 0; i < 9; i++) {
        sleep(1);
    }
    printf("finished\n");
}


void sigusr1_handler(int signum) {
    printf("SIGUSR1\n");
    for (int i = 0; i < 9; i++) {
        sleep(1);
    }
    printf("finished\n");
}


int main(int argc, char *argv[])
{
    printf("PID: %d\n", getpid());
    struct sigaction act, act2;
    memset(&act, 0, sizeof(struct sigaction));
    sigfillset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    act.sa_handler = sigint_handler;
    sigaction(SIGINT, &act, NULL);

    memset(&act2, 0, sizeof(struct sigaction));
    sigfillset(&act2.sa_mask);
    act2.sa_flags = SA_SIGINFO;
    act2.sa_handler = sigusr1_handler;
    sigaction(SIGUSR1, &act2, NULL);
    while(1) {

    }
    exit(0);
}
