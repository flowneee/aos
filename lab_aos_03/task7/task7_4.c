#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int caught = 0;


void handler(int signum)
{
    int wstatus;
    while (1) {
        pid_t pid = waitpid(-1, &wstatus, WNOHANG | WUNTRACED);
        if (pid < 0) {
            caught = 1;
            break;
        }
        printf("Child exited %d with status %d\n", pid, wstatus);
    }
}


int main(int argc, char *argv[])
{
    int wstatus;
    pid_t pid;
    signal(SIGCHLD, handler);
    if (fork() == 0) {
        printf("pause...\n");
        pause();
    } else {
        while (!caught) {}
    }
}
