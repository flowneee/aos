#include <stdio.h>
#include <signal.h>
#include <stdlib.h>


void handler(int signum) {
    printf("SIGINT\n");
    signal(SIGINT, SIG_DFL);
}


int main(int argc, char *argv[])
{
    signal(SIGINT, handler);
    while(1) {

    }
    exit(0);
}
