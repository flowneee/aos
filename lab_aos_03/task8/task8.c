#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int caught = 0;
int child_loops = 100;


void handler(int signum)
{
    int wstatus;
    while (1) {
        pid_t pid = waitpid(-1, &wstatus, WNOHANG | WUNTRACED);
        if (pid < 0) {
            caught = 1;
            break;
        }
        printf("Child exited %d with status %d\n", pid, wstatus);
    }
}


void child_handler(int signum)
{
    printf("\nMax loops value decreased to 50...\n");
    child_loops = 50;
}


int main(int argc, char *argv[])
{
    int wstatus;
    pid_t pid;
    signal(SIGCHLD, handler);
    pid = fork();
    if (pid == 0) {
        signal(SIGUSR1, child_handler);
        for (int i = 0; i < child_loops; i++) {
            printf("%d ", i);
            fflush(stdout);
            for (int j = 0; j < 1000; j++) {
                for (int k = 0; k < 10000; k++) {}
            }
        }
    } else {
        sleep(1);
        kill(pid, SIGUSR1);
        while (!caught) {}
    }
}
