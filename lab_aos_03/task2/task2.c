#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>


void handler(int signum) {
    printf("SIGINT\n");
    struct sigaction act;
    memset(&act, 0, sizeof(struct sigaction));
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    act.sa_handler = SIG_DFL;
    sigaction(SIGINT, &act, NULL);
}


int main(int argc, char *argv[])
{
    struct sigaction act;
    memset(&act, 0, sizeof(struct sigaction));
    sigemptyset(&act.sa_mask);
    // sigaddset(&act.sa_mask, SIGINT);
    act.sa_flags = SA_SIGINFO;
    act.sa_handler = handler;
    sigaction(SIGINT, &act, NULL);
    while(1) {

    }
    exit(0);
}
