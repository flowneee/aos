#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main()
{
    pid_t child = fork();
    if (child) {
        printf("Parent PID: %d\nParent PPID: %d\nParent PGID: %d\n\n", getpid(),
               getppid(), getpgid(0));
        pause();
    } else {
        printf("Child PID: %d\nChild PPID: %d\nChild PGID: %d\n\n", getpid(),
               getppid(), getpgid(0));
        setpgrp();
        pause();
    }
}
