#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>


void copy_fd_content(int in, int out);


int main(int argc, char **argv)
{
    int wstatus = 0;
    int child = fork();
    if (child) {
        int fd = open(argv[1], O_RDONLY);
        if (fd < 0) {
            perror("On open()");
            return 1;
        }
        int out_fd = open("parent.out", O_RDWR | O_CREAT | O_TRUNC, 0777);
        if (out_fd < 0) {
            perror("On open()");
            return 1;
        }
        copy_fd_content(fd, out_fd);
        waitpid(child, &wstatus, 0);
        printf("Content of parent.out:\n");
        lseek(out_fd, SEEK_SET, 0);
        copy_fd_content(out_fd, 1);
    } else {
        int fd = open(argv[1], O_RDONLY);
        if (fd < 0) {
            perror("On open()");
            return 1;
        }
        int out_fd = open("child.out", O_RDWR | O_CREAT | O_TRUNC, 0777);
        if (out_fd < 0) {
            perror("On open()");
            return 1;
        }
        copy_fd_content(fd, out_fd);
        printf("Content of child.out:\n");
        lseek(out_fd, SEEK_SET, 0);
        copy_fd_content(out_fd, 1);
    }
}


void copy_fd_content(int in, int out)
{
    char buffer[BUFSIZ] = { };
    int bytes = 0;
    while (1) {
        bytes = read(in, buffer, sizeof(char) * BUFSIZ);
        if (bytes < 0) {
            perror("read()");
            exit(1);
        }
        if (write(out, buffer, bytes) < 0) {
            perror("write()");
            exit(1);
        }
        if (bytes < (sizeof(char) * BUFSIZ)) {
            break;
        }
    }
}
