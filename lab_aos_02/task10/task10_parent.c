#define _GNU_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>


extern char **environ;


void print_envs();


char *child_args[] = {NULL, "arg1", "arg2", "arg3", NULL};


int main(int argc, char **argv)
{
    int child = fork();
    int wstatus = 1;
    if (child) {
        printf("Parent args:\n");
        for (int i = 1; i < argc; i++) {
            printf("%s\n", argv[i]);
        }
        printf("Parent environment:\n");
        print_envs();
        waitpid(child, &wstatus, 0);
    } else {
        child_args[0] = argv[1];
        execvpe(argv[1], child_args, environ);
    }
}


void print_envs()
{
    for (char **str = environ; *str != NULL; str++) {
        printf("%s\n", *str);
    }
}
