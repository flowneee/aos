#define _GNU_SOURCE


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>


extern char **environ;


void print_envs();


int main(int argc, char **argv)
{
    printf("Child args:\n");
    for (int i = 1; i < argc; i++) {
        printf("%s\n", argv[i]);
    }
    printf("Child environment:\n");
    print_envs();
    return 0;
}


void print_envs()
{
    for (char **str = environ; *str != NULL; str++) {
        printf("%s\n", *str);
    }
}
