#include <string>
#include <iostream>


#define _GNU_SOURCE


#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>


int main(int argc, char **argv, char **envp)
{
    int child = fork();
    if (child) {
        waitpid(child, NULL, 0);
        std::cout << "Execute via 'system':" << std::endl;
        std::string s = "";
        for (int i = 1; i < argc; i++) {
            s += argv[i];
            s += " ";
        }
        system(s.c_str());
    } else {
        std::cout << "Execute via 'execvpe':" << std::endl;
        execvpe(argv[1], argv + 1, envp);
    }
}
