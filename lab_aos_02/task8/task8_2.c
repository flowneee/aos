#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>


void copy_fd_content(int in, int out);


int main(int argc, char **argv)
{
    int fd = open(argv[1], O_RDONLY);
    int wstatus = 0;
    if (fd < 0) {
        perror("On open()");
        return 1;
    }
    int child = fork();
    if (child) {
        printf("Content from parent process:\n");
        copy_fd_content(fd, 1);
        waitpid(child, &wstatus, 0);
    } else {
        printf("Content from child process:\n");
        copy_fd_content(fd, 1);
    }
}


void copy_fd_content(int in, int out)
{
    char buffer[BUFSIZ] = { };
    int bytes = 0;
    while (1) {
        bytes = read(in, buffer, sizeof(char) * BUFSIZ);
        if (bytes < 0) {
            perror("read()");
            exit(1);
        }
        if (write(out, buffer, bytes) < 0) {
            perror("write()");
            exit(1);
        }
        if (bytes < (sizeof(char) * BUFSIZ)) {
            break;
        }
    }
}
