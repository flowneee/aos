#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main()
{
    pid_t child = fork();
    int wstatus = 0;
    if (child) {
        printf("Parent PID: %d\nParent PPID: %d\nParent PGID: %d\n\n", getpid(),
               getppid(), getpgid(0));
        if (waitpid(child, &wstatus, 0) < 0) {
            perror("On waitpid()");
        }
        printf("Child %d result: %d\n", child, WEXITSTATUS(wstatus));
    } else {
        printf("Child PID: %d\nChild PPID: %d\nChild PGID: %d\n\n", getpid(),
               getppid(), getpgid(0));
        sleep(1);
        return 3;
    }
}
