#include <stdio.h>
#include <stdlib.h>


extern char **environ;


void print_envs();


int main(int args, char **argv)
{
    printf("Before changing:\n");
    print_envs();
    printf("\n\n");

    printf("Adding MY_OWN_ENV variable...\n\n\n");
    if (setenv("MY_OWN_ENV", "my own value", 1)) {
        perror("On setenv()");
    }

    printf("After changing:\n");
    print_envs();
}


void print_envs()
{
    for (char **str = environ; *str != NULL; str++) {
        printf("%s\n", *str);
    }
}
