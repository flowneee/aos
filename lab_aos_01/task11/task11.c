#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>


int main(int argc, char *argv[])
{
    struct stat sb;
    off_t max_size = 0;
    int max_index = 1;
    for (int i = 1; i < argc; i++) {
        if (stat(argv[i], &sb) < 0) {
            perror("stat()");
            exit(1);
        }
        if (sb.st_size > max_size) {
            max_size = sb.st_size;
            max_index = i;
        }
    }
    printf("Max size: %lld bytes (file: %s)\n", (long long) max_size,
           argv[max_index]);
    return 0;
}
