#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


void copy_fd_content();


int main(int argc, char *argv[])
{
    if (argc > 2) {
        int in = 0;
        int out = 0;
        if ((in = open(argv[1], O_RDONLY)) < 0) {
            perror("open() src");
            exit(1);
        }
        if (posix_fadvise(in, 0, 0, POSIX_FADV_SEQUENTIAL) < 0) {
            perror("Kernel advise failed");
        }
        if ((out = open(argv[2], O_RDWR | O_CREAT | O_TRUNC, 0755)) < 0) {
            perror("open() dst");
            exit(1);
        }
        if (dup2(in, 0) < 0) {
            perror("dup2() src->0");
            exit(1);
        }
        if (dup2(out, 1) < 0) {
            perror("dup2() dst->1");
            exit(1);
        }
        copy_fd_content();
        if (close(in) < 0) {
            perror("close() src");
        }
        if (close(out) < 0) {
            perror("close() dst");
        }
    } else {
        char buffer;
        while (1) {
            read(0, &buffer, sizeof(char));
            write(1, &buffer, sizeof(char));
        }
    }
    return 0;
}


void copy_fd_content()
{
    char buffer[BUFSIZ] = { };
    int bytes = 0;
    while (1) {
        bytes = read(0, buffer, sizeof(char) * BUFSIZ);
        if (bytes < 0) {
            perror("read()");
            exit(1);
        }
        if (write(1, buffer, bytes) < 0) {
            perror("write()");
            exit(1);
        }
        if (bytes < (sizeof(char) * BUFSIZ)) {
            break;
        }
    }
}
