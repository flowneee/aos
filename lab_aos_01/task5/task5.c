#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


int read_file(int fd);


int main(int argc, char *argv[])
{
    int access = strtoul(argv[2], NULL, 8);
    umask(0);
    int fd = open(argv[1], O_RDWR | O_CREAT | O_TRUNC, access);
    int result = 0;
    char buffer[256] = { };
    printf("Creating file\n");
    printf("File descriptor: %d\n", fd);
    if (fd < 0) {
        perror("On open()");
    }
    
    result = lseek(fd, 1048576, SEEK_SET);
    printf("Current position: %d\n", result);
    if (result < 0) {
        perror("On write()");
    }
    
    result = write(fd, "One\nTwo\nThree\n", 14);
    printf("Bytes written: %d\n", result);
    if (result < 0) {
        perror("On write()");
    }
    
    if (close(fd)) {
        perror("On close()");
    }
    return 0;
}
