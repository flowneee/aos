#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


void copy_fd_content();


int main(int argc, char *argv[])
{
    int fd = 0;
    if ((fd = open(argv[1], O_RDONLY)) < 0) {
        perror("open()");
        exit(1);
    }
    off_t position = lseek(fd, -1, SEEK_END);
    if (position < 0) {
        perror("lseek() initial");
        exit(1);
    }
    char buffer;
    while (1) {
        if (read(fd, &buffer, sizeof(char)) < 0) {
            perror("read()");
            exit(1);
        }
        printf("%c", buffer);
        position = lseek(fd, -2, SEEK_CUR);
        if (position < 0) {
            if (errno != EINVAL) {
                perror("lseek()");
            }
            return 1;
        }

    }
    return 0;
}
