./a.out

./a.out non-existent-file

ln -s ./symlink1 ./symlink2
ln -s ./symlink2 ./symlink1
./a.out symlink1
rm ./symlink1 ./symlink2

touch access_error
chmod 000 ./access_error
./a.out access_error
rm -f access_error

./a.out a.out/file

./a.out aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
