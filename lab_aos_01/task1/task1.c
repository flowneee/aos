#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
    printf("Opening file '%s'\n", argv[1]);
    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) {
        printf("Errno value: %d\n", errno);
        printf("Via sys_errorlist: %s\n", sys_errlist[errno]);
        perror("Via perror(): ");
        return 1;
    }
    close(fd);
    return 0;
}
