#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


int read_file(int fd);


int main(int argc, char *argv[])
{
    int access = strtoul(argv[2], NULL, 8);
    umask(0);
    int fd = open(argv[1], O_RDONLY | O_CREAT, access);
    printf("Creating file\n");
    printf("%d\n", fd);
    if (fd < 0) {
        perror("On open()");
    }
    int result = write(fd, "One\nTwo\nThree\n", 14);
    printf("%d\n", result);
    if (result < 0) {
        perror("On write()");
    }
    result = close(fd);
    printf("%d\n", result);
    if (result) {
        perror("On close()");
    }
    printf("Reading file\n");
    fd = open(argv[1], O_RDONLY);
    printf("%d\n", fd);
    if (fd < 0) {
        perror("On open()");
    }
    read_file(fd);
    result = close(fd);
    printf("%d\n", result);
    if (result) {
        perror("On close()");
    }
    printf("Opening file\n");
    fd = open(argv[1], O_RDWR);
    printf("Result of last open(): %d\n", fd);
    if (fd < 0) {
        perror("On open()");
    }
    result = close(fd);
    printf("%d\n", result);
    if (result) {
        perror("On close()");
    }
    return 0;
}


int read_file(int fd)
{
    ssize_t readed = 0, overall = 0;
    size_t CHUNK_SIZE = 1024;
    char* buffer = (char*)malloc(CHUNK_SIZE * sizeof(char) + 1);
    buffer[CHUNK_SIZE] = '\0';
    while (1) {
        readed = read(fd, buffer, CHUNK_SIZE);
        printf("%d,", readed);
        if (readed < 0) {
            perror("On read()");
            free(buffer);
            return -1;
        }
        overall += readed;
        if (readed < CHUNK_SIZE) {
            buffer[readed] = '\0';
            printf("%s\n", buffer);
            free(buffer);
            return overall;
        } else {
            printf("%s", buffer);
        }
    }
}
