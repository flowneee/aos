#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define _GNU_SOURCE

void copy_file(char *src, char *dst);


int main(int argc, char *argv[])
{
    if (argc > 2) {
        copy_file(argv[1], argv[2]);
    } else {
        char buffer;
        while (1) {
            read(0, &buffer, sizeof(char));
            write(1, &buffer, sizeof(char));
        }
    }
    return 0;
}


void copy_file(char *src, char *dst)
{
    int in = 0;
    int out = 0;
    char buffer[BUFSIZ] = { };
    if ((in = open(src, O_RDONLY)) < 0) {
        perror("open() src");
        exit(1);
    }
    if (posix_fadvise(in, 0, 0, POSIX_FADV_SEQUENTIAL) < 0) {
        perror("Kernel advise failed");
    }
    if ((out = open(dst, O_RDWR | O_CREAT | O_TRUNC, 0755)) < 0) {
        perror("open() dst");
        exit(1);
    }
    int bytes = 0;
    while (1) {
        bytes = read(in, buffer, sizeof(char) * BUFSIZ);
        if (bytes < 0) {
            perror("read()");
            exit(1);
        }
        if (write(out, buffer, bytes) < 0) {
            perror("write()");
            exit(1);
        }
        if (bytes < (sizeof(char) * BUFSIZ)) {
            break;
        }
    }
    if (close(in) < 0) {
        perror("close() src");
    }
    if (close(out) < 0) {
        perror("close() dst");
    }

}
