#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <map>
#include <string>
#include <iostream>
#include <pthread.h>


const int HANDSHAKE = 1;


struct Message {
    long mtype;
    char mtext[256];
};


void add_new_client(std::map<int, int> &clients, char text[]);
void dispatch(std::map<int, int> &clients, Message *msg);


int main(int argc, char *argv[])
{
    std::map<int, int> clients = {};
    Message msg;
    int msqid = msgget(atoi(argv[1]), IPC_CREAT | 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    std::cout << "Server message queue key: " << atoi(argv[1])
              << std::endl;
    while (true) {
        ssize_t readed = msgrcv(msqid, &msg, 256, 0, 0);
        if (readed < 0) {
            perror("on msgrcv()");
            exit(1);
        }
        switch (msg.mtype) {
        case HANDSHAKE:
            add_new_client(clients, msg.mtext);
            break;
        default:
            dispatch(clients, &msg);
            break;
        }
    }
    exit(0);
}


void add_new_client(std::map<int, int> &clients, char text[])
{
    int client_key = atoi(text);
    int msqid = msgget(client_key, 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    } else {
        clients[client_key] = msqid;
        std::cout << "New client connected. Message queue key: "
                  << client_key << std::endl;
    }
}


void dispatch(std::map<int, int> &clients, Message *msg)
{
    std::cout << "Income message. To: " << msg->mtype << " " << msg->mtext
              << std::endl;
    if (msgsnd(clients[msg->mtype], msg, 256, 0) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
}
