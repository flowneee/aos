#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <map>
#include <string>
#include <iostream>
#include <pthread.h>
#include <thread>


const int HANDSHAKE = 1;


struct Message {
    long mtype;
    char mtext[256];
};


int main(int argc, char *argv[])
{
    int server_key = atoi(argv[1]);
    int client_key = atoi(argv[2]);
    Message msg;
    // create own
    int msqid = msgget(client_key, IPC_CREAT | 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    // notify server
    int server_msqid = msgget(server_key, 0666);
    if (server_msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    msg.mtype = HANDSHAKE;
    strcpy(msg.mtext, std::to_string(client_key).c_str());
    if (msgsnd(server_msqid, &msg, 256, IPC_NOWAIT) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
    std::cout << "Client message queue key: " << client_key
              << std::endl;
    // start listening thread
    std::thread listening_thread([&msqid, &argv]() {
            while (true) {
                Message msg;
                ssize_t readed = msgrcv(msqid, &msg, 256, 0, 0);
                if (readed < 0) {
                    perror("on msgrcv()");
                    exit(1);
                }
                std::cout << msg.mtype << ": " << msg.mtext << std::endl;
            }
        });
    // w8 for messages from stdin and send them to server
    while (true) {
        std::cout << "Enter destination key:" << std::endl;
        std::cin >> msg.mtype;
        std::cout << "Enter text:" << std::endl;
        std::cin >> msg.mtext;
        if (msgsnd(server_msqid, &msg, 256, IPC_NOWAIT) != 0) {
            perror("on msgsnd()");
            exit(1);
        }
    }
    exit(0);
}
