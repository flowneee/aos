#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <map>
#include <string>
#include <iostream>
#include <pthread.h>


struct Message {
    long mtype;
    char mtext[256];
};


void add_new_client(std::map<int, int> &clients, char text[]);
void dispatch(std::map<int, int> &clients, Message *msg);


int main(int argc, char *argv[])
{
    Message msg;
    int msqid = msgget(atoi(argv[1]), IPC_CREAT | 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    std::cout << "Server message queue key: " << atoi(argv[1])
              << std::endl;
    while (true) {}
    exit(0);
}
