#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

typedef struct Message {
    long mtype;
    char mtext[256];
} Message;


int main(int argc, char *argv[])
{
    int msqid = msgget(atoi(argv[1]), IPC_CREAT | 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    printf("Message queue %d ID: %d\n", atoi(argv[1]), msqid);
    struct msqid_ds ds = {0};
    if (msgctl(msqid, IPC_STAT, &ds) != 0) {
        perror("on msgctl()");
        exit(1);
    }
    printf("msg_stime:     %ld\n", ds.msg_stime);
    printf("msg_rtime:     %ld\n", ds.msg_rtime);
    printf("msg_ctime:     %ld\n", ds.msg_ctime);
    printf("__msg_cbytes:  %ld\n", ds.__msg_cbytes);
    printf("msg_qnum:      %ld\n", ds.msg_qnum);
    printf("msg_bytes:     %ld\n", ds.msg_qbytes);
    printf("msg_lspid:     %d\n", ds.msg_lspid);
    printf("msg_lrpid:     %d\n", ds.msg_lrpid);
    printf("msg_perm:\n");
    printf("\t__key:     %d\n", ds.msg_perm.__key);
    printf("\tuid:       %d\n", ds.msg_perm.uid);
    printf("\tgid:       %d\n", ds.msg_perm.gid);
    printf("\tcuid:      %d\n", ds.msg_perm.cuid);
    printf("\tcgid:      %d\n", ds.msg_perm.cgid);
    printf("\tmode:      %d\n", ds.msg_perm.mode);
    printf("\t__seq:     %d\n", ds.msg_perm.__seq);
    Message msg = { 1, "message 1" };
    if (msgsnd(msqid, &msg, 256,
               IPC_NOWAIT) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
    Message msg2 = { 2, "message 2" };
    if (msgsnd(msqid, &msg2, 256,
               IPC_NOWAIT) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
    Message msg3 = { 3, "message 3" };
    if (msgsnd(msqid, &msg3, 256,
               IPC_NOWAIT) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
    exit(0);
}
