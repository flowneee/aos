#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>


int main(int argc, char *argv[])
{
    int msqid = msgget(atoi(argv[1]), 0);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    if (msgctl(msqid, IPC_RMID, NULL) != 0) {
        perror("on msgget()");
        exit(1);
    }
    exit(0);
}
