#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

typedef struct Message {
    long mtype;
    char mtext[256];
} Message;


int main(int argc, char *argv[])
{
    int msqid = msgget(atoi(argv[1]), 0);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    Message msg = {0};
    while (1) {
        ssize_t readed = msgrcv(msqid, &msg, 256, 1, IPC_NOWAIT);
        if (readed < 0) {
            perror("on msgrcv()");
            exit(1);
        }
        printf("Message (type %ld): %s\n", msg.mtype, msg.mtext);
    }
    exit(0);
}
