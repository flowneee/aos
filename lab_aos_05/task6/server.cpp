#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <map>
#include <string>
#include <iostream>
#include <pthread.h>


const int HANDSHAKE = 1;


struct Message {
    long mtype;
    char mtext[256];
};


void add_new_client(std::map<int, int> &clients, char text[]);
void respond(std::map<int, int> &clients, Message *msg);


int main(int argc, char *argv[])
{
    std::map<int, int> clients = {};
    Message msg;
    int msqid = msgget(atoi(argv[1]), IPC_CREAT | 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    std::cout << "Server message queue key: " << atoi(argv[1])
              << std::endl;
    while (true) {
        ssize_t readed = msgrcv(msqid, &msg, 256, 0, 0);
        if (readed < 0) {
            perror("on msgrcv()");
            exit(1);
        }
        switch (msg.mtype) {
        case HANDSHAKE:
            add_new_client(clients, msg.mtext);
            break;
        default:
            std::cout << msg.mtype << ": " << msg.mtext << std::endl;
            respond(clients, &msg);
            break;
        }
    }
    exit(0);
}


void add_new_client(std::map<int, int> &clients, char text[])
{
    int msqid = msgget(std::atoi(text), 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    } else {
        clients[std::atoi(text)] = msqid;
        std::cout << "New client connected. Message queue key: "
                  << std::atoi(text) << std::endl;
    }
}


void respond(std::map<int, int> &clients, Message *msg)
{
    std::string s = msg->mtext;
    s = "(echo): " + s;
    strcpy(msg->mtext, s.c_str());
    if (msgsnd(clients[msg->mtype], msg, 256, IPC_NOWAIT) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
}
