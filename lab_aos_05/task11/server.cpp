#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <map>
#include <string>
#include <iostream>
#include <pthread.h>


struct Message {
    long mtype;
    char mtext[sizeof(int)];
};


struct LongMessage{
    long mtype;
    char mtext[sizeof(long int)];
};


int main(int argc, char *argv[])
{
    Message msg;
    int msqid = msgget(atoi(argv[1]), IPC_CREAT | 0666);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    std::cout << "Server message queue key: " << atoi(argv[1])
              << std::endl;
    while (true) {
        Message f,s;
        ssize_t readed = msgrcv(msqid, &f, sizeof(int), 1, 0);
        if (readed < 0) {
            perror("on msgrcv()");
            exit(1);
        }
        readed = msgrcv(msqid, &s, sizeof(int), 2, 0);
        if (readed < 0) {
            perror("on msgrcv()");
            exit(1);
        }
        LongMessage lmsg;
        lmsg.mtype = 3;
        *reinterpret_cast<long int*>(lmsg.mtext) =
            *reinterpret_cast<int*>(f.mtext) * *reinterpret_cast<int*>(s.mtext);
        if (msgsnd(msqid, &lmsg, sizeof(long int), IPC_NOWAIT) != 0) {
            perror("on msgsnd()");
            exit(1);
        }
        std::cout << *reinterpret_cast<int*>(f.mtext) << " * "
            << *reinterpret_cast<int*>(s.mtext) << " = "
            << *reinterpret_cast<long int*>(lmsg.mtext) << std::endl;
    }
    exit(0);
}
