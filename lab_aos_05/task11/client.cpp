#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <map>
#include <string>
#include <iostream>
#include <pthread.h>
#include <thread>


struct Message {
    long mtype;
    char mtext[sizeof(int)];
};


struct LongMessage{
    long mtype;
    char mtext[sizeof(long int)];
};


int main(int argc, char *argv[])
{
    int server_key = atoi(argv[1]);
    int msqid = msgget(server_key, 0);
    if (msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    Message f,s;
    f.mtype = 1;
    s.mtype = 2;
    std::cout << "Enter first number: ";
    std::cin >> *reinterpret_cast<int*>(f.mtext);
    std::cout << "Enter second number: ";
    std::cin >> *reinterpret_cast<int*>(s.mtext);
    if (msgsnd(msqid, &f, sizeof(int), IPC_NOWAIT) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
    if (msgsnd(msqid, &s, sizeof(int), IPC_NOWAIT) != 0) {
        perror("on msgsnd()");
        exit(1);
    }
    std::cout << "Waiting for result..." << std::endl;
    LongMessage lmsg;
    ssize_t readed = msgrcv(msqid, &lmsg, sizeof(long int), 3, 0);
    if (readed < 0) {
        perror("on msgrcv()");
        exit(1);
    }
    std::cout << "Result: " << *reinterpret_cast<long int*>(lmsg.mtext)
              << std::endl;
    exit(0);
}
