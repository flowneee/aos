#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <map>
#include <string>
#include <iostream>
#include <pthread.h>


const int SHUTDOWN = 1;


struct Message {
    long mtype;
    char mtext[256];
};


void add_new_client(std::map<int, int> &clients, char text[]);
void dispatch(std::map<int, int> &clients, Message *msg);


int main(int argc, char *argv[])
{
    Message msg;
    int in_msqid = msgget(atoi(argv[1]), IPC_CREAT | 0666);
    if (in_msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    int out_msqid = msgget(atoi(argv[1]) + 1, IPC_CREAT | 0666);
    if (out_msqid < 0) {
        perror("on msgget()");
        exit(1);
    }
    std::cout << "Server message queue key: " << atoi(argv[1])
              << std::endl;
    while (true) {
        ssize_t readed = msgrcv(in_msqid, &msg, 256, 0, 0);
        if (readed < 0) {
            perror("on msgrcv()");
            exit(1);
        }
        if (msgsnd(out_msqid, &msg, 256, 0) != 0) {
            perror("on msgsnd()");
            exit(1);
        }
    }
    exit(0);
}
