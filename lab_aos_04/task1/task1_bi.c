#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


#define PING "ping "
#define PONG ": pong\n"


int main(int argc, char *argv[])
{
    int fd1[2];
    int fd2[2];
    int counter = 20;
    pid_t pid;
    char buf;
    pipe(fd1);
    pipe(fd2);
    pid = fork();
    if (pid == 0) {
        close(fd1[1]);
        close(fd2[0]);
        while (read(fd1[0], &buf, 1) > 0) {
            printf(PONG);
            fflush(stdout);
            write(fd2[1], "0", 1);
        }
        printf("Child exit...\n");
        close(fd2[1]);
        close(fd1[0]);
    } else {
        close(fd1[0]);
        close(fd2[1]);
        while (counter > 0) {
            printf(PING);
            fflush(stdout);
            write(fd1[1], "0", 1);
            read(fd2[0], &buf, 1);
            counter--;
        }
        printf("Parent tired...\n");
        close(fd1[1]);
        close(fd2[0]);
        wait(NULL);
    }
}
