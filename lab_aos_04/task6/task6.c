#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    char buf;
    int fd = open(argv[1], O_RDONLY | O_NONBLOCK);
    printf("Result of read with O_NONBLOCK: %ld\n", read(fd, &buf, 1));
    close(fd);
    fd = open(argv[1], O_RDONLY);
    while (read(fd, &buf, 1) > 0) {
        write(1, &buf, 1);
    }
}
