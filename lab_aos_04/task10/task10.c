#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int pid = fork();
    sleep(1);
    char buf;
    while (1) {
        if (read(0, &buf, 1)) {
            if (pid) {
                printf("(child)");
                write(1, &buf, 1);
                printf("\n");
            } else {
                printf("(parent)");
                write(1, &buf, 1);
                printf("\n");
            }
            fflush(stdout);
        }
    }
}
