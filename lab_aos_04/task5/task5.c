#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char **argv)
{
    int fd[2];
    pipe(fd);
    if (fork())
    {
        dup2(fd[1], 1);
        close(fd[1]);
        close(fd[0]);
        execlp("who", "who", NULL);
    } else {
        dup2(fd[0], 0);
        close(fd[0]);
        close(fd[1]);
        execlp("wc", "wc", "-l", NULL);
    }
}
