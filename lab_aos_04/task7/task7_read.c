#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int fd = open(argv[1], O_RDONLY);
    char buf;
    while (1) {
        if (read(fd, &buf, 1)) {
            write(1, &buf, 1);
            fflush(stdout);
        }
    }
}
