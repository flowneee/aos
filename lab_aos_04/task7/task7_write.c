#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int fd = open(argv[1], O_RDWR | O_CREAT, 0777);
    lseek(fd, 0, SEEK_END);
    while(write(fd, "1", 1)) {
        sleep(1);
    }
}
