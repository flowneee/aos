#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int fd1[2];
    int fd2[2];
    pid_t pid;
    char buf;
    pipe(fd1);
    pipe(fd2);
    pid = fork();
    if (pid == 0) {
        close(fd1[1]);
        close(fd2[0]);
        while (read(fd1[0], &buf, 1) > 0) {
            write(fd2[1], &buf, 1);
        }
        printf("Child exit...\n");
        close(fd2[1]);
        close(fd1[0]);
    } else {
        close(fd1[0]);
        close(fd2[1]);
        while (1) {
            read(0, &buf, 1);
            write(fd1[1], &buf, 1);
            if (read(fd2[0], &buf, 1) < 0) {
                break;
            }
            write(1, &buf, 1);
        }
        printf("Parent exit...\n");
        close(fd1[1]);
        close(fd2[0]);
        wait(NULL);
    }
}
