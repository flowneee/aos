#include <sys/types.h>
#include <sys/wait.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <string>
#include <iostream>


int main(int argc, char *argv[])
{
    int fd[2];
    pid_t pid;
    char buf;
    pipe(fd);
    std::cout << fd[0] << " " << fd[1] << std::endl;
    pid = fork();
    if (pid == 0) {
        execlp("./b.out", "./b.out", std::to_string(fd[0]).c_str(),
               std::to_string(fd[1]).c_str(), NULL);
    } else {
        close(fd[1]);
        while (read(fd[0], &buf, 1) > 0) {
            write(1, &buf, 1);
        }
        write(1, "\n", 1);
        close(fd[0]);
        wait(NULL);
    }
}
