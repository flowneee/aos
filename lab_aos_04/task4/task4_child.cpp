#include <sys/types.h>
#include <sys/wait.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <string>
#include <sstream>
#include <iostream>


int main(int argc, char *argv[])
{
    int fd[2];
    char buf;
    std::stringstream ss;
    ss << argv[1];
    ss >> fd[0];
    ss << argv[2];
    ss >> fd[1];
    std::cout << fd[0] << " " << fd[1] << std::endl;
    close(fd[0]);
    while (read(0, &buf, 1) > 0) {
        write(fd[1], &buf, 1);
    }
    write(1, "\n", 1);
    close(fd[1]);
}
