#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    struct flock lck;
    lck.l_type = F_WRLCK;
    lck.l_whence = 0;
    lck.l_start = 0L;
    lck.l_len = 0L;
    if (fcntl (0, F_SETLK, &lck)) {
        printf("%d", getpid());
        perror(" on fcntl()");
        exit(1);
    }
    if (fcntl (1, F_SETLK, &lck)) {
        printf("%d", getpid());
        perror(" on fcntl()");
        exit(1);
    }
    int pid = fork();
    sleep(1);
    char buf;
    if (pid) {
        if (fcntl (0, F_SETLK, &lck)) {
            printf("%d", getpid());
            perror(" on fcntl()");
            exit(1);
        }
        if (fcntl (1, F_SETLK, &lck)) {
            printf("%d", getpid());
            perror(" on fcntl()");
            exit(1);
        }
    }
    while (1) {
        if (read(0, &buf, 1)) {
            if (pid) {
                printf("(child)");
                write(1, &buf, 1);
                printf("\n");
            } else {
                printf("(parent)");
                write(1, &buf, 1);
                printf("\n");
            }
            fflush(stdout);
        }
    }
}
