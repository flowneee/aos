#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int fd[2];
    pid_t pid;
    char buf;
    pipe(fd);
    pid = fork();
    if (pid == 0) {
        close(fd[0]);
        while (read(0, &buf, 1) > 0) {
            write(fd[1], &buf, 1);
        }
        write(1, "\n", 1);
        close(fd[1]);
    } else {
        close(fd[1]);
        while (read(fd[0], &buf, 1) > 0) {
            write(1, &buf, 1);
        }
        write(1, "\n", 1);
        close(fd[0]);
        wait(NULL);
    }
}
