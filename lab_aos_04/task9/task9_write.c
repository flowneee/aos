#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int fd = open(argv[1], O_RDWR | O_CREAT, 0777);
    struct flock lck;
    lck.l_type = F_WRLCK;
    lck.l_whence = 0;
    lck.l_start = 0L;
    lck.l_len = 0L;
    fcntl (fd, F_SETLK, &lck);
    lseek(fd, 0, SEEK_END);
    while(1) {
        int res = write(fd, "1", 1);
        if (res < 1) {
            break;
        }
        sleep(1);
    }
}
