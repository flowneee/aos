#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int fd = open(argv[1], O_RDONLY);
    char buf;
    struct flock lck;
    lck.l_type = F_RDLCK;
    lck.l_whence = 0;
    lck.l_start = 0L;
    lck.l_len = 0L;
    if (fcntl (fd, F_SETLK, &lck)) {
        perror("on fcntl()");
        exit(1);
    }
    while (1) {
        if (read(fd, &buf, 1)) {
            write(1, &buf, 1);
            fflush(stdout);
        }
    }
}
